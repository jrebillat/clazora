package net.alantea.clazora.gui.sprints;

import java.awt.Color;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;

import net.alantea.clazora.data.Sprint;
import net.alantea.clazora.data.Task;
import net.alantea.glide.Element;
import net.alantea.glide.GException;
import net.alantea.glide.GlideElement;
import net.alantea.glide.Glider;
import net.alantea.glide.PossibleChild;
import net.alantea.glideui.GliderUi;
import net.alantea.glideui.panels.GUIElementPanel;
import net.alantea.glideui.panels.ParentElements;
import net.alantea.horizon.message.Messenger;
import net.alantea.swing.Application;
import net.alantea.swing.tree.NavigationTree;
import net.alantea.utils.MultiMessages;

public class Sprints extends ParentElements
{
   private static final long serialVersionUID = 1L;
   
   static
   {
      GUIElementPanel.addInitializationMethod(Sprint.class, Sprints::initializeRootTreeNode);
   }

   public Sprints(GliderUi ui)
   {
      super(ui, Sprint.class);
      setBackground(Color.WHITE);
      Application.addSelectionListener(Sprint.class, (o, v) -> show((Sprint) v));
      this.setMasterType(new PossibleChild(ui.getGlider(), Sprint.class));

      Messenger.addSubscription(null, "changeSprintTasks", this);
   }

   public void goToSprint(Sprint sprint)
   {
      getGliderUi().showBaseContainerContentPane();
      getGliderUi().getVerticalTabFolder().select("Sprints");
      Application.setGlobalSelection(sprint);
   }
   
   public boolean allowChild(Object parent, Object child)
   {
      return false;
   }

   public void manageMenu(NavigationTree tree, JPopupMenu menu, Sprint data)
   {
      manageMenu(tree, menu, (GlideElement)data);
   }

   public static DefaultMutableTreeNode initializeRootTreeNode(GliderUi ui, Element element)
   {
      return initializeSprintsRootTreeNode(ui.getGlider());
   }

   public DefaultMutableTreeNode initializeRootTreeNode(GliderUi ui, Class<?> refClass)
   {
      return initializeRootTreeNode(refClass);
   }

   public DefaultMutableTreeNode initializeRootTreeNode(Glider glider, Class<?> refClass)
   {
      if (refClass == Sprint.class)
      {
         return initializeSprintsRootTreeNode(glider);
      }
      return null;
   }

   protected static DefaultMutableTreeNode initializeSprintsRootTreeNode(Glider glider)
   {
      DefaultMutableTreeNode root = new DefaultMutableTreeNode(MultiMessages.get("Sprints.tab.title"));
      try
      {
         List<Sprint> list = new LinkedList<>();
         list.addAll(Sprint.getAllSprints(glider));
         Collections.sort(list, (a, b) -> {return a.getName().compareTo(b.getName());}); 
         if (list != null)
         {
            for(Sprint sprint : list)
            {
               DefaultMutableTreeNode node = new DefaultMutableTreeNode(sprint); 
               root.add(node);
               addSubTasks(sprint, node);
            }
         }
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
      return root;
   }

   public static void addSubTasks(Sprint sprint, DefaultMutableTreeNode sprintNode)
   {
      List<Task> tasks = sprint.getTasks();
      Collections.sort(tasks, (a, b) -> {return a.getName().compareTo(b.getName());}); 
      if (tasks != null)
      {
         for(Task t : tasks)
         {
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(t); 
            sprintNode.add(node);
         }
      }
   }

   @Override
   public DefaultMutableTreeNode initializeRootTreeNode(Class<?> refClass)
   {
      // TODO Auto-generated method stub
      return null;
   }
}
