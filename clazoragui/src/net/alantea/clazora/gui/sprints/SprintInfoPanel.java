package net.alantea.clazora.gui.sprints;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JPanel;

import net.alantea.clazora.data.Sprint;
import net.alantea.clazora.gui.ClazoraGui;
import net.alantea.swing.layout.percent.PercentConstraints;
import net.alantea.swing.layout.percent.PercentLayout;
import net.alantea.swing.misc.LabeledButton;
import net.alantea.swing.text.LabeledLabel;

@SuppressWarnings("serial")
public class SprintInfoPanel extends JPanel
{
   
   public SprintInfoPanel(ClazoraGui ui, Sprint sprint)
   {
      setLayout(new BorderLayout());

      // Title
      LabeledButton title = new LabeledButton("TaskInfoPanel.title.action", () -> ui.getGuiElements().getSprints().goToSprint(sprint) );
      title.setLabelText(sprint.getName());
      Font f = title.getFont();
      title.setFont(f.deriveFont(f.getStyle() | Font.BOLD, (int)(f.getSize() * 1.5)));
      add(title, BorderLayout.NORTH);
      
      JPanel main = new JPanel();
      main.setLayout(new PercentLayout());
      add(main, BorderLayout.CENTER);
      
      // Date
      LabeledLabel cdateLabel = new LabeledLabel();
      cdateLabel.setLabelText("Sprint.tab.startDate");
      cdateLabel.setText(sprint.getStartDate().toString());
      main.add(cdateLabel, new PercentConstraints(0.25, 0.0, 0.33, 1.0));
      
      // Date
      LabeledLabel ddateLabel = new LabeledLabel();
      ddateLabel.setLabelText("Sprint.tab.dueDate");
      ddateLabel.setText(sprint.getDueDate().toString());
      main.add(ddateLabel, new PercentConstraints(0.66, 0.0, 0.33, 1.0));
   }

}
