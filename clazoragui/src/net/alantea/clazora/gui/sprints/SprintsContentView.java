package net.alantea.clazora.gui.sprints;

import java.util.LinkedList;
import java.util.List;

import net.alantea.clazora.data.Sprint;
import net.alantea.clazora.gui.ClazoraGui;
import net.alantea.swing.panels.ListedView;

@SuppressWarnings("serial")
public class SprintsContentView extends ListedView
{
   public SprintsContentView()
   {
      this.setElementHeight(0);
      setTitleText("Sprints");
   }

   public void updateContent(ClazoraGui ui, List<Sprint> list)
   {
      List<SprintInfoPanel> panelsList = new LinkedList<>();
      for (Sprint sprint : list)
      {
         SprintInfoPanel sprintPanel = new SprintInfoPanel(ui, sprint);
         panelsList.add(sprintPanel);
      }
      setContent(panelsList);
   }

}
