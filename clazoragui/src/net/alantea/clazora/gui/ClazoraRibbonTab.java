package net.alantea.clazora.gui;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ButtonGroup;

import net.alantea.clazora.data.Task;
import net.alantea.clazora.data.TaskType;
import net.alantea.clazora.data.WorkflowState;
import net.alantea.clazora.gui.tasks.TasksContentView;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;
import net.alantea.glideui.GliderUi;
import net.alantea.glideui.utils.GliderRibbon;
import net.alantea.swing.action.ActionManager;
import net.alantea.swing.action.ActionMultipleSelector;
import net.alantea.swing.action.ActionRadioButton;
import net.alantea.swing.action.ActionToggleButton;
import net.alantea.swing.action.ManagedSelection;
import net.alantea.swing.pageelements.Redrawable;
import net.alantea.swing.ribbon.RibbonGroup;
import net.alantea.swing.ribbon.RibbonTab;
import net.alantea.utils.MultiMessages;
import net.alantea.utils.StreamLister;
import net.alantea.utils.exception.LntException;


public class ClazoraRibbonTab extends RibbonTab implements Redrawable
{
   static
   {
      MultiMessages.addBundle("net.alantea.clazora.gui.Clazora");
   }
   
   private static final long serialVersionUID = 1L;
   private GliderUi gui;
   private TasksContentView pane;
   private boolean updating = false;
   private ActionMultipleSelector<TaskType> typesSelection;
   private ActionMultipleSelector<String> statesSelection;
   private ActionRadioButton fromlatest;
   private ActionRadioButton fromoldest;
   private ActionToggleButton lateToggle;
   private ActionRadioButton fromdlatest;
   private ActionRadioButton fromdoldest;
   
   public ClazoraRibbonTab(GliderUi gui, GliderRibbon ribbon, TasksContentView pane)
   {
      super(ribbon, "clazora-tasks", "ListsFilterRibbon.clazora", () -> gui.showCardPane("clazora-tasks"));
      this.gui = gui;
      this.pane = pane;

      RibbonGroup selectiongrp = new RibbonGroup(this, "Ribbon.filters.clazora.selection");
      //--------------
      ManagedSelection.putListener("Ribbon.filters.clazora.bytype", null, this::showClazora2);
      typesSelection = new ActionMultipleSelector<TaskType>(selectiongrp, null, "Ribbon.filters.clazora.bytype");
      setTypesSelectionItems(gui.getGlider());
      selectiongrp.add(typesSelection);
      //-------------- 
      ManagedSelection.putListener("Ribbon.filters.clazora.bystate", null, this::showClazora2);
      statesSelection = new ActionMultipleSelector<String>(selectiongrp, null, "Ribbon.filters.clazora.bystate");
      setStatesSelectionItems(gui.getGlider());
      //--------------
      ActionManager.instrumentAction("Ribbon.filters.clazora.late", null, gui, this::showClazora);
      lateToggle = new ActionToggleButton(selectiongrp, null, "Ribbon.filters.clazora.late");
      lateToggle.setSelected(false);
      //--------------
      
      RibbonGroup ordergrp = new RibbonGroup(this, "Ribbon.filters.clazora.order");
      //--------------
      ButtonGroup group = new ButtonGroup();
      ActionManager.instrumentAction("Ribbon.filters.clazora.order.latest.creation", null, gui, this::showClazora);
      fromlatest = new ActionRadioButton(ordergrp, null, "Ribbon.filters.clazora.order.latest.creation");
      fromlatest.setSelected(true);
      group.add(fromlatest);
      //--------------
      ActionManager.instrumentAction("Ribbon.filters.clazora.order.oldest.creation", null, gui, this::showClazora);
      fromoldest = new ActionRadioButton(ordergrp, null, "Ribbon.filters.clazora.order.oldest.creation");
      group.add(fromoldest);
      //--------------
      ActionManager.instrumentAction("Ribbon.filters.clazora.order.latest.due", null, gui, this::showClazora);
      fromdlatest = new ActionRadioButton(ordergrp, null, "Ribbon.filters.clazora.order.latest.due");
      group.add(fromdlatest);
      //--------------
      ActionManager.instrumentAction("Ribbon.filters.clazora.order.oldest.due", null, gui, this::showClazora);
      fromdoldest = new ActionRadioButton(ordergrp, null, "Ribbon.filters.clazora.order.oldest.due");
      group.add(fromdoldest);
      //--------------
      
      showClazora(gui.getGlider());
   }

   private void setTypesSelectionItems(Glider glider)
   {
      try
      {
         typesSelection.setItems(TaskType.getTaskTypes(glider));
      }
      catch (GException e)
      {
         new LntException("Error setting type items", e);
      }
   }

   private void setStatesSelectionItems(Glider glider)
   {
      try
      {
         statesSelection.setItems(WorkflowState.getAllWorkflowStateNames(glider));
      }
      catch (GException e)
      {
         new LntException("Error setting state items", e);
      }
   }

   private void showClazora(Object glider)
   {
      showClazora(((GliderUi) glider).getGlider());
   }

   private void showClazora(Glider glider)
   {
         if (!updating)
         {
            List<Task> output = new LinkedList<>();
            try
            {
               output = Task.getAllTasks(glider);
            }
            catch (GException e)
            {
               new LntException("Error getting all tasks", e);
            }

            if (typesSelection != null)
            {
               List<TaskType> selectedIx = typesSelection.getSelectedItems();
               if ((selectedIx != null) && (!selectedIx.isEmpty()))
               {
                  output = StreamLister.getByFilter(output, (a) -> selectedIx.contains(a.getType()));
               }
            }

            if (statesSelection != null)
            {
               List<String> stateNames = statesSelection.getSelectedItems();
               if ((stateNames != null) && (!stateNames.isEmpty()))
               {
                  output = StreamLister.getByFilter(output, (a) -> stateNames.contains(a.getState().getName()));
               }
            }
            if ((lateToggle != null) && (lateToggle.isSelected()))
            {
               Date date = new Date();
               output = StreamLister.getByFilter(output, (a) -> a.getDueDate().getTime() < date.getTime());
            }
            if ((fromlatest != null) && (fromlatest.isSelected()))
            {
               output = StreamLister.getByComparison(output, (a, b) -> b.getCreationDate().compareTo(a.getCreationDate()));
            }
            else if ((fromoldest != null) && (fromoldest.isSelected()))
            {
               output = StreamLister.getByComparison(output, (a, b) -> a.getCreationDate().compareTo(b.getCreationDate()));
            }
            else if ((fromdlatest != null) && (fromdlatest.isSelected()))
            {
               output = StreamLister.getByComparison(output, (a, b) -> b.getDueDate().compareTo(a.getDueDate()));
            }
            else if ((fromdoldest != null) && (fromdoldest.isSelected()))
            {
               output = StreamLister.getByComparison(output, (a, b) -> a.getDueDate().compareTo(b.getDueDate()));
            }
            if (pane != null)
            {
               pane.updateContent(gui, output);
            }
            gui.showCardPane("clazora-tasks");
            gui.refresh();
         }
   }

   private void showClazora2(Object v)
   {
      showClazora(gui);
   }

   @Override
   public void doRedraw()
   {
      showClazora(gui);
   }
}
