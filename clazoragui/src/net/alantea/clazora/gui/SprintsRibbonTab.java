package net.alantea.clazora.gui;

import java.util.LinkedList;
import java.util.List;

import javax.swing.ButtonGroup;

import net.alantea.clazora.data.Sprint;
import net.alantea.clazora.data.TaskType;
import net.alantea.clazora.data.WorkflowState;
import net.alantea.clazora.gui.sprints.SprintsContentView;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;
import net.alantea.glideui.utils.GliderRibbon;
import net.alantea.swing.action.ActionManager;
import net.alantea.swing.action.ActionMultipleSelector;
import net.alantea.swing.action.ActionRadioButton;
import net.alantea.swing.action.ActionToggleButton;
import net.alantea.swing.action.ManagedSelection;
import net.alantea.swing.ribbon.RibbonGroup;
import net.alantea.swing.ribbon.RibbonTab;
import net.alantea.utils.MultiMessages;
import net.alantea.utils.exception.LntException;


public class SprintsRibbonTab extends RibbonTab
{
   static
   {
      MultiMessages.addBundle("net.alantea.clazora.gui.Clazora");
   }
   
   private static final long serialVersionUID = 1L;
   private ClazoraGui gui;
   private SprintsContentView pane;
   private boolean updating = false;
   private ActionMultipleSelector<TaskType> typesSelection;
   private ActionMultipleSelector<String> statesSelection;
   private ActionRadioButton fromlatest;
   private ActionRadioButton fromoldest;
   private ActionToggleButton lateToggle;
   private ActionRadioButton fromdlatest;
   private ActionRadioButton fromdoldest;
   
   public SprintsRibbonTab(ClazoraGui gui, GliderRibbon ribbon, SprintsContentView pane)
   {
      super(ribbon, "clazora-sprints", "ListsFilterRibbon.clazora.sprints", () -> gui.showCardPane("clazora-sprints"));
      this.gui = gui;
      this.pane = pane;

      RibbonGroup selectiongrp = new RibbonGroup(this, "Ribbon.filters.clazora.sprints.selection");
      //--------------
// TODO
      ManagedSelection.putListener("Ribbon.filters.clazora.sprints.bytype", null, ((SprintsRibbonTab)this)::showClazora3);
      typesSelection = new ActionMultipleSelector<TaskType>(selectiongrp, null, "Ribbon.filters.clazora.bytype");
      setTypesSelectionItems(gui.getGlider());
      selectiongrp.add(typesSelection);
      //-------------- 
      ManagedSelection.putListener("Ribbon.filters.clazora.sprints.bystate", null, this::showClazora3);
      statesSelection = new ActionMultipleSelector<String>(selectiongrp, null, "Ribbon.filters.clazora.bystate");
      setStatesSelectionItems(gui.getGlider());
      //--------------
      ActionManager.instrumentAction("Ribbon.filters.clazora.sprints.late", null, gui, this::showClazora);
      lateToggle = new ActionToggleButton(selectiongrp, null, "Ribbon.filters.clazora.late");
      lateToggle.setSelected(false);
      //--------------
      
      RibbonGroup ordergrp = new RibbonGroup(this, "Ribbon.filters.clazora.sprints.order");
      //--------------
      ButtonGroup group = new ButtonGroup();
      ActionManager.instrumentAction("Ribbon.filters.clazora.sprints.order.latest.creation", null, gui, this::showClazora);
      fromlatest = new ActionRadioButton(ordergrp, null, "Ribbon.filters.clazora.order.latest.creation");
      fromlatest.setSelected(true);
      group.add(fromlatest);
      //--------------
      ActionManager.instrumentAction("Ribbon.filters.clazora.sprints.order.oldest.creation", null, gui, this::showClazora);
      fromoldest = new ActionRadioButton(ordergrp, null, "Ribbon.filters.clazora.order.oldest.creation");
      group.add(fromoldest);
      //--------------
      ActionManager.instrumentAction("Ribbon.filters.clazora.order.sprints.latest.due", null, gui, this::showClazora);
      fromdlatest = new ActionRadioButton(ordergrp, null, "Ribbon.filters.clazora.order.latest.due");
      group.add(fromdlatest);
      //--------------
      ActionManager.instrumentAction("Ribbon.filters.clazora.order.sprints.oldest.due", null, gui, this::showClazora);
      fromdoldest = new ActionRadioButton(ordergrp, null, "Ribbon.filters.clazora.order.oldest.due");
      group.add(fromdoldest);
      //--------------
      
      showClazora(gui);
   }

   private void setTypesSelectionItems(Glider glider)
   {
      try
      {
         typesSelection.setItems(TaskType.getTaskTypes(glider));
      }
      catch (GException e)
      {
         new LntException("Error setting type items", e);
      }
   }

   private void setStatesSelectionItems(Glider glider)
   {
      try
      {
         statesSelection.setItems(WorkflowState.getAllWorkflowStateNames(glider));
      }
      catch (GException e)
      {
         new LntException("Error setting state items", e);
      }
   }

   private void showClazora(Object glider)
   {
      showClazora(((ClazoraGui) glider).getGlider());
   }

   private void showClazora(Glider glider)
   {
         if (!updating)
         {
            List<Sprint> output = new LinkedList<>();
            try
            {
               output = Sprint.getAllSprints(glider);
            }
            catch (GException e)
            {
               new LntException("Error getting all sprints", e);
            }
//
//            if (typesSelection != null)
//            {
//               List<TaskType> selectedIx = typesSelection.getSelectedItems();
//               if ((selectedIx != null) && (!selectedIx.isEmpty()))
//               {
//                  output = Lister.getElementsByFilter(output, (a) -> selectedIx.contains(a.getType()));
//               }
//            }
//
//            if (statesSelection != null)
//            {
//               List<String> stateNames = statesSelection.getSelectedItems();
//               if ((stateNames != null) && (!stateNames.isEmpty()))
//               {
//                  output = Lister.getElementsByFilter(output, (a) -> stateNames.contains(a.getState().getName()));
//               }
//            }
//            if ((lateToggle != null) && (lateToggle.isSelected()))
//            {
//               Date date = new Date();
//               output = Lister.getElementsByFilter(output, (a) -> a.getDueDate().getTime() < date.getTime());
//            }
//            if ((fromlatest != null) && (fromlatest.isSelected()))
//            {
//               output = Lister.getElementsByComparison(output, (a, b) -> b.getCreationDate().compareTo(a.getCreationDate()));
//            }
//            else if ((fromoldest != null) && (fromoldest.isSelected()))
//            {
//               output = Lister.getElementsByComparison(output, (a, b) -> a.getCreationDate().compareTo(b.getCreationDate()));
//            }
//            else if ((fromdlatest != null) && (fromdlatest.isSelected()))
//            {
//               output = Lister.getElementsByComparison(output, (a, b) -> b.getDueDate().compareTo(a.getDueDate()));
//            }
//            else if ((fromdoldest != null) && (fromdoldest.isSelected()))
//            {
//               output = Lister.getElementsByComparison(output, (a, b) -> a.getDueDate().compareTo(b.getDueDate()));
//            }
            pane.updateContent(gui, output);
            gui.showCardPane("clazora-sprints");
            gui.refresh();
         }
   }

   private void showClazora2()
   {
      showClazora(gui);
   }

   private void showClazora3(Object any)
   {
      showClazora2();
   }
}
