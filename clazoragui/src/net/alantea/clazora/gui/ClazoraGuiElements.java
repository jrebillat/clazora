package net.alantea.clazora.gui;

import java.awt.BorderLayout;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

import net.alantea.clazora.Clazora;
import net.alantea.clazora.data.PossibleSubTask;
import net.alantea.clazora.data.Task;
import net.alantea.clazora.gui.projects.Projects;
import net.alantea.clazora.gui.sprints.Sprints;
import net.alantea.clazora.gui.tasks.Tasks;
import net.alantea.clazora.gui.utils.LabelBasedTreeCellRenderer;
import net.alantea.glide.GException;
import net.alantea.glideui.GliderUi;
import net.alantea.glideui.pageelements.GliderPage;
import net.alantea.glideui.parser.GUIDataAccessParser;
import net.alantea.swing.misc.ResourceManager;
import net.alantea.swing.misc.SwingUtils;
import net.alantea.swing.panels.StackFolder;
import net.alantea.swing.panels.VerticalTabFolder;
import net.alantea.utils.MultiMessages;
import net.alantea.utils.StreamLister;

public class ClazoraGuiElements
{
   static
   {
      MultiMessages.addBundle("net.alantea.clazora.gui.Clazora");
   }

   private GliderUi gui;
   private StackFolder stack;
   private Projects projects;
   private Tasks tasks;
   private Sprints sprints;
   

   
   public Tasks startTasks(GliderUi gui)
   {
      this.gui = gui;
      
      GUIDataAccessParser parser = new GUIDataAccessParser();
      parser.load(GliderPage.class.getClassLoader().getResourceAsStream("net/alantea/clazora/gui/dataaccess.xml"));
      
      tasks = new Tasks(gui);
      tasks.setMasterType(new PossibleSubTask(gui.getGlider(), Clazora.getMasterTaskType(gui.getGlider()).getName()));
      projects = new Projects(gui);
      sprints = new Sprints(gui);
      if (gui.getVerticalTabFolder() instanceof VerticalTabFolder)
      {
         ((VerticalTabFolder) gui.getVerticalTabFolder()).addFolder(tasks, "Tasks", ResourceManager.getImage("Tasks.png"), "VerticalFolder.clazora.tooltip");
         ((VerticalTabFolder) gui.getVerticalTabFolder()).addFolder(projects, "Projects", ResourceManager.getImage("Projects.png"), "VerticalFolder.clazora.tooltip");
      }
      else if (gui.getVerticalTabFolder() instanceof StackFolder)
      {
         ((StackFolder) gui.getVerticalTabFolder()).addFolder(tasks, "Tasks");
         ((StackFolder) gui.getVerticalTabFolder()).addFolder(projects, "Projects");
      }
      LabelBasedTreeCellRenderer.setIcon("Tasks", "Tasks.png");
      LabelBasedTreeCellRenderer.setIcon("Epic", "epic.png");
      LabelBasedTreeCellRenderer.setIcon("Story", "story.png");
      LabelBasedTreeCellRenderer.setIcon("Task", "task.png");
      LabelBasedTreeCellRenderer.setIcon("Project", "project.png");
      LabelBasedTreeCellRenderer.setIcon("Sprint", "toDoList.png");
      tasks.getTree().setTreeCellRenderer(new LabelBasedTreeCellRenderer());
      projects.getTree().setTreeCellRenderer(new LabelBasedTreeCellRenderer());
      
      return tasks;
   }

   public void showLateTasksDialog()
   {
      Date now = new Date();
      try
      {
         List<Task> lateTasks = StreamLister.getByFilter(Task.getAllTasks(gui.getGlider()),
               (t) ->  (t.getDueDate().getTime() < now.getTime()) && !(t.getState().getName().equals("Done")));
         if (!lateTasks.isEmpty())
         {
            lateTasks = StreamLister.getByComparison (
                  lateTasks, (a, b) -> b.getDueDate().compareTo(a.getDueDate()));

            JPanel panel = new JPanel(new BorderLayout());

            JLabel label = new JLabel(MultiMessages.get("ClazoraGui.latepanel.title"));
            label.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            panel.add(label, BorderLayout.NORTH);

            JList<Task> list = new JList<Task>();
            panel.add(list, BorderLayout.CENTER);

            DefaultListModel<Task> listModel = new DefaultListModel<>();
            for (Task task : lateTasks)
            {
               listModel.addElement(task);
            }
            list.setModel(listModel);
            SwingUtils.showInformPanelDialog(null, "ClazoraGui.latepanel", panel);
         }
      }
      catch (GException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      };
      

   }
   
   public StackFolder getStack()
   {
      if (stack == null)
      {
         stack = new StackFolder();
      }
      return stack;
   }
   public Projects getProjects()
   {
      return projects;
   }
   public Tasks getTasks()
   {
      return tasks;
   }
   public Sprints getSprints()
   {
      return sprints;
   }

}
