package net.alantea.clazora.gui;


import javax.swing.JMenuBar;

import net.alantea.clazora.Clazora;
import net.alantea.clazora.gui.menu.MainMenuBar;
import net.alantea.clazora.gui.projects.ProjectsContentView;
import net.alantea.clazora.gui.sprints.SprintsContentView;
import net.alantea.clazora.gui.tasks.TasksContentView;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;
import net.alantea.glideui.GliderUi;
import net.alantea.glideui.utils.GliderRibbon;
import net.alantea.swing.panels.ControlledTabbed;
import net.alantea.swing.ribbon.Ribbon;
import net.alantea.swing.ribbon.RibbonTab;
import net.alantea.utils.MultiMessages;


@SuppressWarnings("serial")
public final class ClazoraGui extends GliderUi
{
   private static Clazora clazora = new Clazora();

   private ClazoraGuiElements guiElements;

   public static void main( String[] args )
   {
      MultiMessages.setFirstBundle("net.alantea.clazora.gui.Clazora");
      MultiMessages.addBundle("net.alantea.glideui.GliderUi");
      try {
         GliderUi gui = null;
         Glider glider = null;
         if (args.length == 1)
         {
            glider = clazora.applyGlider(args[0]);
            gui = new ClazoraGui(glider);
         }
         else 
         {
            glider = GliderUi.chooseGlider("BddChoose.type", "BddChoose.label", "clz");
            clazora.applyGlider(glider);
            gui = new ClazoraGui(glider);
            gui.showCardPane();
         }

         if (gui != null)
         {
            gui.start();
         }
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Create the frame.
    */
   private ClazoraGui(Glider glider)
   {
      super(glider);
      guiElements.getStack().select(0);
      
      this.addCardPane("clazora-tasks", new TasksContentView());
      this.addCardPane("clazora-sprints", new SprintsContentView());
      this.addCardPane("clazora-projects", new ProjectsContentView());
   }

   public ControlledTabbed getVerticalTabFolder()
   {
      if (guiElements == null)
      {
         guiElements = new ClazoraGuiElements();
         guiElements.startTasks(this);
      }
      return guiElements.getStack();
   }
   
   protected Ribbon loadRibbon()
   {
      GliderRibbon ribbon = new GliderRibbon();
      RibbonTab elements = new RibbonTab(ribbon, "elements", "Ribbon.elements", () -> ClazoraGui.this.showCardPane());
      ribbon.instrumentContentPaneAction(this, elements, "Tasks");
      ribbon.instrumentContentPaneAction(this, elements, "Projects");
      new ClazoraRibbonTab(this, ribbon,new TasksContentView());
      new SprintsRibbonTab(this, ribbon,new SprintsContentView());
      return ribbon;
   }
   
   protected JMenuBar loadMainMenuBar()
   {
      return new MainMenuBar(getGlider());
   }
   
   public ClazoraGuiElements getGuiElements()
   {
      return guiElements;
   }
}
