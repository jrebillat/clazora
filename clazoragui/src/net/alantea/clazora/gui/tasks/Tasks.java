package net.alantea.clazora.gui.tasks;

import java.awt.Color;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;

import net.alantea.clazora.data.Sprint;
import net.alantea.clazora.data.Task;
import net.alantea.clazora.data.TaskType;
import net.alantea.clazora.data.WorkflowState;
import net.alantea.glide.Element;
import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.GlideElement;
import net.alantea.glideui.GliderUi;
import net.alantea.glideui.panels.GUIElementPanel;
import net.alantea.glideui.panels.ParentElements;
import net.alantea.horizon.message.Messenger;
import net.alantea.horizon.message.Receive;
import net.alantea.swing.Application;
import net.alantea.swing.misc.SwingUtils;
import net.alantea.swing.tree.NavigationTree;
import net.alantea.swing.tree.TreeRootLabel;
import net.alantea.utils.MultiMessages;
import net.alantea.utils.exception.LntException;

public class Tasks extends ParentElements
{
   private static final long serialVersionUID = 1L;
   
   public static final String CLAZORA_TASKS_PANEL = "CLAZORA_TASKS_PANEL";
   
   static
   {
      GUIElementPanel.addInitializationMethod(Task.class, Tasks::initializeRootTreeNode);
   }

   public Tasks(GliderUi ui)
   {
      super(ui, Task.class);
      ui.putReference(CLAZORA_TASKS_PANEL, this);
      setBackground(Color.WHITE);
      Application.addSelectionListener(Task.class, (o, v) -> show((Task) v));

      Messenger.addSubscription(null, "ChangeTaskState", this);
      Messenger.addSubscription(null, "ResetTaskState", this);
   }
   
   public static Tasks getTasks(GliderUi ui)
   {
      Object object = ui.getReference(Tasks.CLAZORA_TASKS_PANEL);
      return (object == null) ? null : (Tasks)object; 
   }

   public void goToTask(Task task)
   {
      getGliderUi().showBaseContainerContentPane();
      getGliderUi().getVerticalTabFolder().select("Tasks");
      Application.setGlobalSelection(task);
   }
   
   public boolean allowChild(Object parent, Object child)
   {
      return ((parent instanceof Task) && (child instanceof Sprint));
   }

   public void manageMenu(NavigationTree tree, JPopupMenu menu, Task data)
   {
      manageMenu(tree, menu, (GlideElement)data);
   }

   public static DefaultMutableTreeNode initializeRootTreeNode(GliderUi gui, Element element)
   {
      return initializeTasksRootTreeNode(gui);
   }

   public DefaultMutableTreeNode initializeRootTreeNode(Class<?> refClass)
   {
      if (refClass == Task.class)
      {
         return initializeTasksRootTreeNode(getGliderUi());
      }
      return null;
   }

   protected static DefaultMutableTreeNode initializeTasksRootTreeNode(GliderUi gui)
   {
      DefaultMutableTreeNode root = initializeTreeRootNode("Tasks");
      try
      {
         List<Task> list = new LinkedList<>();
         list.addAll(Task.getAllTasks(gui.getGlider()));
         Collections.sort(list, (a, b) -> {return a.getName().compareTo(b.getName());}); 
         if (list != null)
         {
            for(Task task : list)
            {
               if (task.getParent() == null)
               {
                  DefaultMutableTreeNode node = new DefaultMutableTreeNode(task); 
                  root.add(node);
                  addSubTasks(task, node);
               }
            }
         }
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
      return root;
   }

   public static DefaultMutableTreeNode initializeTreeRootNode(String key)
   {
      TreeRootLabel rootLabel = new TreeRootLabel(MultiMessages.get(key + ".tab.title"), MultiMessages.get(key + ".tab.image"));
      DefaultMutableTreeNode root = new DefaultMutableTreeNode(rootLabel);
      return root;
   }

   public static void addSubTasks(Task task, DefaultMutableTreeNode taskNode)
   {
      List<Task> tasks = task.getSubTasks();
      Collections.sort(tasks, (a, b) -> {return a.getName().compareTo(b.getName());}); 
      if (tasks != null)
      {
         for(Task t : tasks)
         {
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(t); 
            taskNode.add(node);
            addSubTasks( t, node);
         }
      }
   }
   
   @Receive(message="ChangeTaskState")
   private void changeTaskState(Entity entity)
   {
      if ((entity != null) && (entity instanceof Task))
      {
         Task task = (Task) entity;
         try
         {
            WorkflowState futureState = (WorkflowState) SwingUtils.showListSelectionDialog(null, 
                  "WorkflowState.select",
                  WorkflowState.getNextWorkflowStates(task.getState()));
            if (futureState != null)
            {
               Task.changeTaskState(entity.getGlider(), task, futureState);
            }
         }
         catch (GException e)
         {
            new LntException("Error changing task state for : " + task.getName(), e);
         }
      }
   }
   
   @Receive(message="ResetTaskState")
   private void resetTaskState(Entity entity)
   {
      if ((entity != null) && (entity instanceof Task))
      {
         Task task = (Task) entity;
         try
         {
            TaskType type = task.getType();
            Task.changeTaskState(entity.getGlider(), task, type.getStartingWorkflowState());
         }
         catch (GException e)
         {
            new LntException("Error resetting task state for : " + task.getName(), e);
         }
      }
   }
}
