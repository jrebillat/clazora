package net.alantea.clazora.gui.tasks;

import java.util.LinkedList;
import java.util.List;

import net.alantea.clazora.data.Task;
import net.alantea.glideui.GliderUi;
import net.alantea.swing.panels.ListedView;

@SuppressWarnings("serial")
public class TasksContentView extends ListedView
{
   private static final int ELEMENTHEIGHT = 0;
   
   public TasksContentView()
   {
      this.setElementHeight(ELEMENTHEIGHT);
   }

   public void updateContent(GliderUi ui, List<Task> list)
   {
      List<TaskInfoPanel> panelsList = new LinkedList<>();
      for (Task task : list)
      {
         TaskInfoPanel taskPanel = new TaskInfoPanel(ui, task);
         panelsList.add(taskPanel);
      }
      setContent(panelsList);
      doLayout();
   }
}
