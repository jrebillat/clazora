package net.alantea.clazora.gui.tasks;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.alantea.clazora.data.Task;
import net.alantea.glideui.GliderUi;
import net.alantea.swing.layout.percent.PercentConstraints;
import net.alantea.swing.layout.percent.PercentLayout;
import net.alantea.swing.misc.LabeledButton;
import net.alantea.swing.text.LabeledLabel;

@SuppressWarnings("serial")
public class TaskInfoPanel extends JPanel
{
   
   public TaskInfoPanel(GliderUi ui, Task task)
   {
      setLayout(new BorderLayout());
      setSize(400, 60);
      setBorder(BorderFactory.createLineBorder(Color.RED));
      // Title
      LabeledButton title = new LabeledButton("TaskInfoPanel.title.action", () -> Tasks.getTasks(ui).goToTask(task));
      title.setLabelText(task.getName());
      Font f = title.getFont();
      title.setFont(f.deriveFont(f.getStyle() | Font.BOLD, (int)(f.getSize() * 1.5)));
      add(title, BorderLayout.NORTH);
      
      JPanel main = new JPanel();
      main.setLayout(new PercentLayout());
      add(main, BorderLayout.CENTER);
      
      // state
      JLabel stateLabel = new JLabel();
      stateLabel.setText(task.getState().getName());
      main.add(stateLabel, new PercentConstraints(0.0, 0.0, 0.25, 1.0));
      
      // Date
      LabeledLabel cdateLabel = new LabeledLabel();
      cdateLabel.setLabelText("Task.tab.creationDate");
      cdateLabel.setText(task.getCreationDate().toString());
      main.add(cdateLabel, new PercentConstraints(0.25, 0.0, 0.33, 1.0));
      
      // Date
      LabeledLabel ddateLabel = new LabeledLabel();
      ddateLabel.setLabelText("Task.tab.dueDate");
      ddateLabel.setText(task.getDueDate().toString());
      main.add(ddateLabel, new PercentConstraints(0.66, 0.0, 0.33, 1.0));
   }

}
