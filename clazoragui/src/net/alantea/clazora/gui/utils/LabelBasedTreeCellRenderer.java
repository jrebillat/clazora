package net.alantea.clazora.gui.utils;

import java.awt.Component;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import net.alantea.clazora.data.Project;
import net.alantea.clazora.data.Sprint;
import net.alantea.clazora.data.Task;
import net.alantea.clazora.data.TaskType;
import net.alantea.glide.ParentElement;
import net.alantea.swing.misc.ResourceManager;
import net.alantea.swing.tree.TreeRootLabel;

@SuppressWarnings("serial")
public class LabelBasedTreeCellRenderer extends DefaultTreeCellRenderer
{
   private static Map<String, ImageIcon> iconsMap = new HashMap<>();
   
   public Component getTreeCellRendererComponent(JTree tree, Object value,
         boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus)
   {
      super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row,
            hasFocus);
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
      Object userObject = node.getUserObject();
      
      if ((userObject != null) && (userObject instanceof ParentElement))
      {
         setToolTipText(((ParentElement) userObject).getTooltip());
         
      }
      
      if ((userObject != null) && (userObject instanceof Task))
      {
         Task task = (Task) userObject;
         TaskType type = task.getType();
         if (type != null)
         {
            Icon icon = iconsMap.get(type.getName());
            if (icon != null)
            {
               setIcon(icon);
            }
         }
      }
      else if (userObject instanceof Project)
      {
         Icon icon = iconsMap.get("Project");
         if (icon != null)
         {
            setIcon(icon);
         }
      }
      else if (userObject instanceof Sprint)
      {
         Icon icon = iconsMap.get("Sprint");
         if (icon != null)
         {
            setIcon(icon);
         }
      }
      else if (userObject instanceof TreeRootLabel)
      {
         Icon icon = iconsMap.get("Tasks");
         if (icon != null)
         {
            setIcon(icon);
         }
      }
      return this;
   }
   
   public static void setIcon(String name, String imageKey)
   {
      Image image = ResourceManager.getImage(imageKey);
      if (image != null)
      {
         iconsMap.put(name, ResourceManager.getIcon(image));
      }
   }

}
