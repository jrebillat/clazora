package net.alantea.clazora.gui.menu;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import net.alantea.glide.GException;
import net.alantea.glide.Glider;
import net.alantea.glideui.menu.EmptyMainMenuBar;
import net.alantea.utils.MultiMessages;

@SuppressWarnings("serial")
public class MainMenuBar extends EmptyMainMenuBar
{
   public MainMenuBar(Glider glider)
   {

      JMenu mnMenu = new JMenu("File");
      add(mnMenu);
      
      JMenuItem exitButton = new JMenuItem(MultiMessages.get("MainMenu.exit.label"));
      exitButton.addActionListener((e) -> {
         try
         {
            glider.save();
         }
         catch (GException e1)
         {
            // TODO Auto-generated catch block
            e1.printStackTrace();
         }
         System.exit(0);
      });
      mnMenu.add(exitButton);
   }
}
