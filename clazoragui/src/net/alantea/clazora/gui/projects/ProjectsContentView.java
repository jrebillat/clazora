package net.alantea.clazora.gui.projects;

import java.util.LinkedList;
import java.util.List;

import net.alantea.clazora.data.Project;
import net.alantea.clazora.gui.ClazoraGui;
import net.alantea.swing.panels.ListedView;

@SuppressWarnings("serial")
public class ProjectsContentView extends ListedView
{
   public ProjectsContentView()
   {
      this.setElementHeight(0);
      setTitleText("Sprints");
   }

   public void updateContent(ClazoraGui ui, List<Project> list)
   {
      List<ProjectInfoPanel> panelsList = new LinkedList<>();
      for (Project project : list)
      {
         ProjectInfoPanel projectPanel = new ProjectInfoPanel(ui, project);
         panelsList.add(projectPanel);
      }
      setContent(panelsList);
   }

}
