package net.alantea.clazora.gui.projects;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JPanel;

import net.alantea.clazora.data.Project;
import net.alantea.clazora.gui.ClazoraGui;
import net.alantea.swing.layout.percent.PercentLayout;
import net.alantea.swing.misc.LabeledButton;

@SuppressWarnings("serial")
public class ProjectInfoPanel extends JPanel
{
   
   public ProjectInfoPanel(ClazoraGui ui, Project project)
   {
      setLayout(new BorderLayout());

      // Title
      LabeledButton title = new LabeledButton("ProjectInfoPanel.title.action", () -> ui.getGuiElements().getProjects().gotoProject(project) );
      title.setLabelText(project.getName());
      Font f = title.getFont();
      title.setFont(f.deriveFont(f.getStyle() | Font.BOLD, (int)(f.getSize() * 1.5)));
      add(title, BorderLayout.NORTH);
      
      JPanel main = new JPanel();
      main.setLayout(new PercentLayout());
      add(main, BorderLayout.CENTER);
   }

}
