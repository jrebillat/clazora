package net.alantea.clazora.gui.projects;

import java.awt.Color;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;

import net.alantea.clazora.data.Project;
import net.alantea.clazora.data.Sprint;
import net.alantea.glide.Element;
import net.alantea.glide.GException;
import net.alantea.glide.GlideElement;
import net.alantea.glide.Glider;
import net.alantea.glide.PossibleChild;
import net.alantea.glideui.GliderUi;
import net.alantea.glideui.panels.GUIElementPanel;
import net.alantea.glideui.panels.ParentElements;
import net.alantea.swing.Application;
import net.alantea.swing.tree.NavigationTree;
import net.alantea.utils.MultiMessages;

public class Projects extends ParentElements
{
   private static final long serialVersionUID = 1L;
   
   static
   {
      GUIElementPanel.addInitializationMethod(Project.class, Projects::initializeRootTreeNode);
   }
   

   public Projects(GliderUi ui)
   {
      super(ui, Project.class);
      setBackground(Color.WHITE);
      Application.addSelectionListener(Project.class, (o, v) -> show((Project) v));
      this.setMasterType(new PossibleChild(ui.getGlider(), Project.class));

      // TODO
//      Messenger.addSubscription(null, "changeSprintTasks", this);
   }

   public void gotoProject(Project project)
   {
      getGliderUi().showBaseContainerContentPane();
      getGliderUi().getVerticalTabFolder().select("Projects");
      Application.setGlobalSelection(project);
   }
   
   public boolean allowChild(Object parent, Object child)
   {
      return ((parent instanceof Project) && (child instanceof Sprint));
   }

   public void manageMenu(NavigationTree tree, JPopupMenu menu, Project data)
   {
      manageMenu(tree, menu, (GlideElement)data);
   }

   public static DefaultMutableTreeNode initializeRootTreeNode(GliderUi ui, Element element)
   {
      return initializeProjectsRootTreeNode(ui.getGlider());
   }

   public DefaultMutableTreeNode initializeRootTreeNode(Class<?> refClass)
   {
      if (refClass == Sprint.class)
      {
         return initializeProjectsRootTreeNode(getGliderUi().getGlider());
      }
      return null;
   }

   protected static DefaultMutableTreeNode initializeProjectsRootTreeNode(Glider glider)
   {
      DefaultMutableTreeNode root = new DefaultMutableTreeNode(MultiMessages.get("Projects.tab.title"));
      try
      {
         List<Project> list = new LinkedList<>();
         list.addAll(Project.getAllProjects(glider));
         Collections.sort(list, (a, b) -> {return a.getName().compareTo(b.getName());}); 
         if (list != null)
         {
            for(Project project : list)
            {
               DefaultMutableTreeNode node = new DefaultMutableTreeNode(project); 
               root.add(node);
               addSprints(project, node);
            }
         }
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
      return root;
   }

   public static void addSprints(Project project, DefaultMutableTreeNode sprintNode)
   {
      List<Sprint> sprints = project.getSprints();
      Collections.sort(sprints, (a, b) -> {return a.getName().compareTo(b.getName());}); 
      if (sprints != null)
      {
         for(Sprint sprint: sprints)
         {
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(sprint); 
            sprintNode.add(node);
         }
      }
   }
}
