package net.alantea.clazora;

import java.io.File;

import net.alantea.clazora.data.TaskType;
import net.alantea.clazora.data.WorkflowState;
import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;
import net.alantea.utils.exception.LntException;

/**
 * The Class Clazora.
 */
public class Clazora
{
   
   /** The Constant TASK. */
   public static final String TASK = "Task";
   
   /** The Constant STORY. */
   public static final String STORY = "Story";
   
   /** The Constant MASTERTASKTYPE. */
   public static final String MASTERTASKTYPE = "Epic";
   
   /** The Constant WORKFLOWCREATED. */
   public static final String WORKFLOWCREATED = "Created";
   
   /** The Constant WORKFLOWSTARTED. */
   public static final String WORKFLOWSTARTED = "Started";
   
   /** The Constant WORKFLOWDONE. */
   public static final String WORKFLOWDONE = "Done";
   
   /** The epic type. */
   private static TaskType epicType;
   
   /** The glider. */
   private Glider glider;

   /**
    * Apply glider.
    *
    * @param path the path
    * @return the glider
    * @throws GException the g exception
    */
   public Glider applyGlider(String path) throws GException
   {
      File file = new File(path);
      boolean toInitialize = ! file.exists();
      
      glider = Glider.load(path);
      
      if (toInitialize)
      {
         initialize();
      }
      
      return glider;
   }

   /**
    * Apply glider.
    *
    * @param glider the glider
    * @throws GException the g exception
    */
   public void applyGlider(Glider glider) throws GException
   {
      applyGlider(glider, false);
      if (glider.getClassEntities(TaskType.class.getName()).isEmpty())
      {
         initialize();
      }
   }

   /**
    * Apply glider.
    *
    * @param glider the glider
    * @param init the init
    * @throws GException the g exception
    */
   public void applyGlider(Glider glider, boolean init) throws GException
   {
      this.glider = glider;
      if (init)
      {
         initialize();
      }
   }

   /**
    * Gets the glider.
    *
    * @return the glider
    */
   public Glider getGlider()
   {
      return glider;
   }

   /**
    * Initialize.
    *
    * @throws GException the g exception
    */
   private void initialize() throws GException
   {
      // Basic types
      TaskType taskType = Clazora.initializeType(glider, TASK);
      TaskType storyType = Clazora.initializeType(glider, STORY);
      epicType = Clazora.initializeType(glider, MASTERTASKTYPE);
      epicType.allowSubType(storyType);
      storyType.allowSubType(taskType);
      taskType.allowSubType(taskType);
      
      glider.save();
   }

   /**
    * Initialize types.
    *
    * @param glider the glider
    * @throws GException the g exception
    */
   public static void initializeTypes(Glider glider) throws GException
   {
      TaskType taskType = Clazora.initializeType(glider, TASK);
      TaskType storyType = Clazora.initializeType(glider, STORY);
      epicType = Clazora.initializeType(glider, MASTERTASKTYPE);
      epicType.allowSubType(storyType);
      storyType.allowSubType(taskType);
      taskType.allowSubType(taskType);
   }

   /**
    * Initialize type.
    *
    * @param glider the glider
    * @param name the name
    * @return the task type
    * @throws GException the g exception
    */
   public static TaskType initializeType(Glider glider, String name) throws GException
   {
      // Basic workflow
      TaskType taskType = TaskType.createTaskType(glider, name);

      WorkflowState initialState = WorkflowState.createWorkflowState(glider, WORKFLOWCREATED);
      taskType.setStartingWorkflowState(initialState);

      WorkflowState startedState = WorkflowState.createWorkflowState(glider, WORKFLOWSTARTED);
      initialState.addNextWorkflowState(startedState);

      WorkflowState doneState = WorkflowState.createWorkflowState(glider, WORKFLOWDONE);
      startedState.addNextWorkflowState(doneState);
      
      return taskType;
   }

   /**
    * Gets the master task type.
    *
    * @param glider the glider
    * @return the master task type
    */
   public static TaskType getMasterTaskType(Glider glider)
   {
      if (epicType == null)
      {
         try
         {
            for (Entity entity : glider.getClassEntities(TaskType.class.getName()))
            {
               TaskType type = (TaskType)entity;
               if (type.getName().equals(MASTERTASKTYPE))
               {
                   epicType = type;
               }
            }
         }
         catch (GException e)
         {
            new LntException("Error getting master task type", e);
         }
      }
      return epicType;
   }
}
