package net.alantea.clazora.data;

import net.alantea.glide.Relation;

/**
 * The Class WorkflowTransition.
 */
public class WorkflowTransition extends Relation
{
   
   /** The name. */
   private String name;
   
   /** The description. */
   private String description;
   
   /**
    * Gets the name.
    *
    * @return the name
    */
   public String getName()
   {
      return name;
   }
   
   /**
    * Sets the name.
    *
    * @param name the new name
    */
   public void setName(String name)
   {
      this.name = name;
   }
   
   /**
    * Gets the description.
    *
    * @return the description
    */
   public String getDescription()
   {
      return description;
   }
   
   /**
    * Sets the description.
    *
    * @param description the new description
    */
   public void setDescription(String description)
   {
      this.description = description;
   }
}
