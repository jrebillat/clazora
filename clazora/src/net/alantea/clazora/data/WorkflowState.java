package net.alantea.clazora.data;

import java.util.LinkedList;
import java.util.List;

import net.alantea.glide.Direction;
import net.alantea.glide.GException;
import net.alantea.glide.GlideElement;
import net.alantea.glide.Glider;
import net.alantea.glide.Relation;

/**
 * The Class WorkflowState.
 */
public class WorkflowState extends GlideElement
{
   
   /** The Constant NEXTSTATE. */
   public static final String NEXTSTATE = "nextState";
   
   /** The description. */
   private String description;
   
   /**
    * Instantiates a new workflow state.
    */
   private WorkflowState()
   {
   }
   
   /**
    * Creates the workflow state.
    *
    * @param glider the glider
    * @param taskType the task type
    * @param name the name
    * @param preceding the preceding
    * @return the workflow state
    * @throws GException the g exception
    */
   public static WorkflowState createWorkflowState(Glider glider, TaskType taskType, 
         String name, WorkflowState preceding) throws GException
   {
      WorkflowState ret = createWorkflowState(glider, name);
      preceding.addNextWorkflowState(ret);
      ret.addNextWorkflowState(preceding);
      return ret;
   }
   
   /**
    * Creates the workflow state.
    *
    * @param glider the glider
    * @param name the name
    * @return the workflow state
    * @throws GException the g exception
    */
   public static WorkflowState createWorkflowState(Glider glider, String name) throws GException
   {
      WorkflowState ret = (WorkflowState) glider.createEntity(WorkflowState.class);
      ret.setName(name);
      return ret;
   }
   
   /**
    * Adds the next workflow state.
    *
    * @param nextState the next state
    */
   public void addNextWorkflowState(WorkflowState nextState)
   {
      if (nextState != null)
      {
         connectNextWorkflowState(nextState);
         nextState.connectNextWorkflowState(this);
      }
   }
   
   /**
    * Connect next workflow state.
    *
    * @param nextState the next state
    */
   private void connectNextWorkflowState(WorkflowState nextState)
   {
      if (nextState != null)
      {
         try
         {
            List<Relation> list = getRelations(Direction.OUTGOING, WorkflowState.NEXTSTATE);
            if (!list.isEmpty())
            {
               for (Relation relation : list)
               {
                  if (nextState.equals(relation.getEndEntity()))
                  {
                     return;
                  }
               }
            }
            this.getGlider().createRelation(this, nextState, WorkflowState.NEXTSTATE);
         }
         catch (GException e)
         {
            e.printStackTrace();
         }
      }
   }
   
   /**
    * Gets the next workflow states.
    *
    * @param state the state
    * @return the next workflow states
    * @throws GException the g exception
    */
   public static List<WorkflowState> getNextWorkflowStates(WorkflowState state) throws GException
   {
      return state.getGlider().getRelatedEntities(state, Direction.OUTGOING, WorkflowState.NEXTSTATE);
   }
   
   /**
    * Gets the description.
    *
    * @return the description
    */
   public String getDescription()
   {
      return description;
   }
   
   /**
    * Sets the description.
    *
    * @param description the new description
    */
   public void setDescription(String description)
   {
      this.description = description;
   }

   /**
    * Gets the all workflow state names.
    *
    * @param glider the glider
    * @return the all workflow state names
    * @throws GException the g exception
    */
   public static List<String> getAllWorkflowStateNames(Glider glider) throws GException
   {
         List<WorkflowState> states = glider.getClassEntities(WorkflowState.class.getName());
         List<String> ret = new LinkedList<>();
         for (WorkflowState state : states)
         {
            String name = state.getName();
            if (!ret.contains(name))
            {
               ret.add(name);
            }
            ret.sort(null);
         }
         return ret;
   }
}
