package net.alantea.clazora.data;
import net.alantea.glide.Relation;

/** Tasks included in a sprint. */
public class Included extends Relation
{
   private String description = "";
   
   /** Constructor. */
   public Included() {}

   /** Access to the description.
   @return the description */
   public final String getDescription() { return this.description; }
}
