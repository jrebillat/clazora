package net.alantea.clazora.data;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import net.alantea.glide.Direction;
import net.alantea.glide.Element;
import net.alantea.glide.GException;
import net.alantea.glide.GlideElement;
import net.alantea.glide.Glider;
import net.alantea.glide.ParentElement;
import net.alantea.glide.PossibleChild;
import net.alantea.glide.Relation;
import net.alantea.horizon.message.Messenger;
import net.alantea.liteprops.MapProperty;
import net.alantea.liteprops.Property;
import net.alantea.liteprops.StringProperty;

/**
 * The Class Task.
 */
public class Task extends GlideElement implements ParentElement
{
   
   /** The Constant SUBTASKSLINK. */
   private static final String SUBTASKSLINK = "subtasks";
   
   /** The Constant STATELINK. */
   private static final String STATELINK = "taskState";
   
   /** The Constant TYPELINK. */
   private static final String TYPELINK = "taskType";

   /** The Constant CHANGESTATE. */
   private static final String CHANGESTATE = "changeState";

   /** The Constant CHANGETYPE. */
   private static final String CHANGETYPE = "changeType";

   /** The Constant CHANGEPARENT. */
   private static final String CHANGEPARENT = "changeParent";
   
   /** The description. */
   private StringProperty description = new StringProperty("");
   
   /** The creation date. */
   private Date creationDate = new Date();
   
   /** The author. */
   private String author;
   
   /** The labels. */
   private List<String> labels;
   
   /** The assignee. */
   private StringProperty assignee = new StringProperty("");
   
   /** The due date. */
   private Property<Date> dueDate = new Property<>(new Date());
   
   /** The enter state date. */
   private Date enterStateDate = new Date();
   
   /** The histo. */
   private MapProperty<Date, String> histo = new MapProperty<>();
   
   /**
    * Instantiates a new task.
    */
   private Task()
   {
   }
   
   /**
    * Creates the task.
    *
    * @param glider the glider
    * @param type the type
    * @param title the title
    * @param description the description
    * @param dueDate the due date
    * @return the task
    * @throws GException the g exception
    */
   public static Task createTask(Glider glider, TaskType type, String title, String description, Date dueDate) throws GException
   {
      Task task = glider.createEntity(Task.class);
      task.setTitle(title);
      task.setDescription(description);
      task.setType(type);
      if (dueDate != null)
      {
         task.setDueDate(dueDate);
      }
      task.histo.put(task.creationDate, task.getState().getName());
      return task;
   }

   /**
    * Delete task.
    *
    * @param glider the glider
    * @param task the task
    * @throws GException the g exception
    */
   public static void deleteTask(Glider glider, Task task) throws GException
   {
      if (task != null)
      {
         glider.removeEntity(task);
      }
   }

   /**
    * Change task state.
    *
    * @param glider the glider
    * @param task the task
    * @param state the state
    * @throws GException the g exception
    */
   public static void changeTaskState(Glider glider, Task task, WorkflowState state) throws GException
   {
      if ((task == null) || (state == null))
      {
         return;
      }
      
      TaskType type = task.getType();
      if (type != null)
      {
         List<WorkflowState> nextTaskStates = glider.getRelatedEntities(task.getState(), Direction.OUTGOING, WorkflowState.NEXTSTATE);
         if (nextTaskStates.contains(state))
         {
            task.setState(state);
         }
      }
   }

   /**
    * Gets the all tasks.
    *
    * @param glider the glider
    * @return the all tasks
    * @throws GException the g exception
    */
   public static List<Task> getAllTasks(Glider glider) throws GException
   {
      return glider.getClassEntities(Task.class.getName());
   }

   /**
    * Gets the title.
    *
    * @return the title
    */
   public String getTitle()
   {
      return getName();
   }

   /**
    * Sets the title.
    *
    * @param title the new title
    */
   public void setTitle(String title)
   {
      setName(title);
   }

   /**
    * Gets the description.
    *
    * @return the description
    */
   public String getDescription()
   {
      return description.get();
   }

   /**
    * Sets the description.
    *
    * @param description the new description
    */
   public void setDescription(String description)
   {
      this.description.set(description);
   }

   /**
    * Gets the creation date.
    *
    * @return the creation date
    */
   public Date getCreationDate()
   {
      return creationDate;
   }

   /**
    * Sets the creation date.
    *
    * @param creationDate the new creation date
    */
   public void setCreationDate(Date creationDate)
   {
      this.creationDate = creationDate;
   }

   /**
    * Gets the author.
    *
    * @return the author
    */
   public String getAuthor()
   {
      return author;
   }

   /**
    * Sets the author.
    *
    * @param author the new author
    */
   public void setAuthor(String author)
   {
      this.author = author;
   }

   /**
    * Gets the labels.
    *
    * @return the labels
    */
   public List<String> getLabels()
   {
      return labels;
   }

   /**
    * Sets the labels.
    *
    * @param labels the new labels
    */
   public void setLabels(List<String> labels)
   {
      this.labels = labels;
   }

   /**
    * Gets the assignee.
    *
    * @return the assignee
    */
   public String getAssignee()
   {
      return assignee.get();
   }

   /**
    * Sets the assignee.
    *
    * @param assignee the new assignee
    */
   public void setAssignee(String assignee)
   {
      this.assignee.set(assignee);
   }

   /**
    * Gets the due date.
    *
    * @return the due date
    */
   public Date getDueDate()
   {
      return dueDate.get();
   }

   /**
    * Sets the due date.
    *
    * @param dueDate the new due date
    */
   public void setDueDate(Date dueDate)
   {
      this.dueDate.set(dueDate);
   }

   /**
    * Gets the enter state date.
    *
    * @return the enter state date
    */
   public Date getEnterStateDate()
   {
      return enterStateDate;
   }

   /**
    * Sets the enter state date.
    *
    * @param enterStateDate the new enter state date
    */
   public void setEnterStateDate(Date enterStateDate)
   {
      this.enterStateDate = enterStateDate;
   }

   /**
    * Gets the type.
    *
    * @return the type
    */
   public TaskType getType()
   {
      try
      {
         List<Relation> list = this.getRelations(Direction.OUTGOING, TYPELINK);
         if ((list != null) && (!list.isEmpty()))
         {
            return (TaskType) list.get(0).getEndEntity();
         }
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
      return null;
   }

   /**
    * Sets the type.
    *
    * @param type the new type
    */
   public void setType(TaskType type)
   {
      if (type != null)
      {
         try
         {
            getGlider().removeRelations(this, TYPELINK);
            getGlider().createRelation(this, type, TYPELINK);
            WorkflowState state = type.getStartingWorkflowState();
            setState(state);
            Messenger.sendMessage(this, CHANGETYPE, type);
         }
         catch (GException e)
         {
            e.printStackTrace();
         }
      }
   }

   /**
    * Gets the state.
    *
    * @return the state
    */
   public WorkflowState getState()
   {
      try
      {
         List<Relation> list = this.getRelations(Direction.OUTGOING, STATELINK);
         if ((list != null) && (!list.isEmpty()))
         {
            return (WorkflowState) list.get(0).getEndEntity();
         }
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
      return null;
   }

   /**
    * Sets the state.
    *
    * @param state the new state
    */
   private void setState(WorkflowState state)
   {
      if (state != null)
      {
         try
         {
            getGlider().removeRelations(this, STATELINK);
            getGlider().createRelation(this, state, STATELINK);
            histo.put(new Date(), getState().getName());
            Messenger.sendMessage(this, CHANGESTATE, state);
         }
         catch (GException e)
         {
            e.printStackTrace();
         }
      }
   }

   /**
    * Gets the parent.
    *
    * @return the parent
    */
   public Task getParent()
   {
      List<Relation> list = this.getRelations(Direction.INCOMING, SUBTASKSLINK);
      if (!list.isEmpty())
      {
         try
         {
            return (Task) list.get(0).getStartEntity();
         }
         catch (GException e)
         {
            e.printStackTrace();
         }
      }
      return null;
   }

   /**
    * Sets the parent.
    *
    * @param parent the new parent
    */
   public void setParent(Task parent)
   {
      try
      {
         List<Relation> list = this.getRelations(Direction.INCOMING, SUBTASKSLINK);
         if (list != null)
         {
            for (Relation relation : list)
            {
               getGlider().removeRelation(relation);
            }
         }
         if (parent != null)
         {
            getGlider().createRelation(parent, this, SUBTASKSLINK);
            Messenger.sendMessage(this, CHANGEPARENT, parent);
         }
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Gets the sub tasks.
    *
    * @return the sub tasks
    */
   public List<Task> getSubTasks()
   {
      List<Task> ret = new LinkedList<>();
      List<Relation> list = this.getRelations(Direction.OUTGOING, SUBTASKSLINK);
      if (list != null)
      {
         for (Relation relation : list)
         {
            try
            {
               ret.add((Task) relation.getEndEntity());
            }
            catch (GException e)
            {
               e.printStackTrace();
            }
         }
      }
      return ret;
   }

   /**
    * Adds the sub task.
    *
    * @param subtask the subtask
    */
   public void addSubTask(Task subtask)
   {
      subtask.setParent(this);
   }

   /**
    * Gets the children.
    *
    * @return the children
    * @throws GException the g exception
    */
   @Override
   public List<Element> getChildren() throws GException
   {
      List<Element> ret = new LinkedList<>();
      ret.addAll(getSubTasks());
      return ret;
   }

   /**
    * Gets the possible children.
    *
    * @return the possible children
    */
   @Override
   public List<PossibleChild> getPossibleChildren()
   {
      List<PossibleChild> ret = new LinkedList<>();
      TaskType type = getType();
      List<TaskType> subs = type.getSubTaskTypes();
      for (TaskType subtype : subs)
      {
         ret.add(new PossibleSubTask(getGlider(), subtype.getName()));
      }
      return ret;
   }

   /**
    * Gets the children link name.
    *
    * @param targetClass the target class
    * @return the children link name
    */
   @Override
   public String getChildrenLinkName(Class<? extends Element> targetClass)
   {
      return SUBTASKSLINK;
   }

   /**
    * Gets the simple name.
    *
    * @return the simple name
    */
   @Override
   public String getSimpleName()
   {
      TaskType type = getType();
      return (type == null) ? "" : getType().getName();
   }
   
   /**
    * Gets the tooltip.
    *
    * @return the tooltip
    */
   @Override
   public String getTooltip() {
      String ret = "<html>"
            + "<b>" + getType() + "</b>"
            + "<br>"
            + "<i>" + getState().getName() + "</i>"
            + "<br>"
            + "Created : " + getCreationDate()
            + "<br>"
            + "Due     : " + getDueDate()
            + "<br>"
            + description.get()
            + "</html>";
      return ret;
   }
   
   /**
    * Gets the historical information.
    *
    * @return the historical information
    */
   public MapProperty<Date, String> getHistoricalInformation()
   {
      return histo;
   }
}
