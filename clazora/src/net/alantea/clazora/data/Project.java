package net.alantea.clazora.data;

import java.util.LinkedList;
import java.util.List;

import net.alantea.glide.Direction;
import net.alantea.glide.Element;
import net.alantea.glide.GException;
import net.alantea.glide.GlideElement;
import net.alantea.glide.Glider;
import net.alantea.glide.ParentElement;
import net.alantea.glide.PossibleChild;
import net.alantea.glide.Relation;
import net.alantea.horizon.message.Messenger;
import net.alantea.liteprops.StringProperty;

/**
 * The Class Project.
 */
public class Project extends GlideElement implements ParentElement
{
   
   /** The Constant PROJECTSPRINTLINK. */
   private static final String PROJECTSPRINTLINK = "projectSprints";

   /** The Constant CHANGESPRINTS. */
   private static final String CHANGESPRINTS = "changeSprints";
   
   /** The description. */
   private StringProperty description = new StringProperty("");
   
   /** The labels. */
   private List<String> labels;
   
   /**
    * Instantiates a new project.
    */
   private Project()
   {
   }
   
   /**
    * Creates the sprint.
    *
    * @param glider the glider
    * @param title the title
    * @param description the description
    * @return the project
    * @throws GException the g exception
    */
   public Project createSprint(Glider glider, String title, String description) throws GException
   {
      Project project = glider.createEntity(Project.class);
      project.setTitle(title);
      project.setDescription(description);
      return project;
   }

   /**
    * Delete project.
    *
    * @param glider the glider
    * @param project the project
    * @throws GException the g exception
    */
   public static void deleteProject(Glider glider, Project project) throws GException
   {
      if (project != null)
      {
         glider.removeEntity(project);
      }
   }

   /**
    * Gets the all projects.
    *
    * @param glider the glider
    * @return the all projects
    * @throws GException the g exception
    */
   public static List<Project> getAllProjects(Glider glider) throws GException
   {
      return glider.getClassEntities(Project.class.getName());
   }

   /**
    * Gets the title.
    *
    * @return the title
    */
   public String getTitle()
   {
      return getName();
   }

   /**
    * Sets the title.
    *
    * @param title the new title
    */
   public void setTitle(String title)
   {
      setName(title);
   }

   /**
    * Gets the description.
    *
    * @return the description
    */
   public String getDescription()
   {
      return description.get();
   }

   /**
    * Sets the description.
    *
    * @param description the new description
    */
   public void setDescription(String description)
   {
      this.description.set(description);
   }

   /**
    * Gets the labels.
    *
    * @return the labels
    */
   public List<String> getLabels()
   {
      return labels;
   }

   /**
    * Sets the labels.
    *
    * @param labels the new labels
    */
   public void setLabels(List<String> labels)
   {
      this.labels = labels;
   }
   
   /**
    * Gets the sprints.
    *
    * @return the sprints
    */
   public List<Sprint> getSprints()
   {
      List<Sprint> ret = new LinkedList<>();
      List<Relation> list = this.getRelations(Direction.OUTGOING, PROJECTSPRINTLINK);
      if (list != null)
      {
         for (Relation relation : list)
         {
            try
            {
               ret.add((Sprint) relation.getEndEntity());
            }
            catch (GException e)
            {
               e.printStackTrace();
            }
         }
      }
      return ret;
   }

   /**
    * Adds the sprint.
    *
    * @param sprint the sprint
    */
   public void addSprint(Sprint sprint)
   {
      try
      {
         List<Relation> list = sprint.getRelations(Direction.INCOMING, PROJECTSPRINTLINK);
         if (list != null)
         {
            for (Relation relation : list)
            {
               getGlider().removeRelation(relation);
            }
         }
         getGlider().createRelation(this, sprint, PROJECTSPRINTLINK);
         Messenger.sendMessage(this, CHANGESPRINTS, sprint);
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Removes the sprint.
    *
    * @param sprint the sprint
    */
   public void removeSprint(Sprint sprint)
   {
      try
      {
         List<Relation> list = sprint.getRelations(Direction.INCOMING, PROJECTSPRINTLINK);
         if (list != null)
         {
            for (Relation relation : list)
            {
               getGlider().removeRelation(relation);
            }
         }
         Messenger.sendMessage(this, CHANGESPRINTS, sprint);
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Gets the children.
    *
    * @return the children
    * @throws GException the g exception
    */
   @Override
   public List<Element> getChildren() throws GException
   {
      List<Element> ret = new LinkedList<Element>();
      ret.addAll(getSprints());
      return ret;
   }

   /**
    * Gets the possible children.
    *
    * @return the possible children
    */
   @Override
   public List<PossibleChild> getPossibleChildren()
   {
      List<PossibleChild> ret = new LinkedList<>();
      ret.add(new PossibleChild(getGlider(), Sprint.class));
      return ret;
   }

   /**
    * Gets the children link name.
    *
    * @param targetClass the target class
    * @return the children link name
    */
   @Override
   public String getChildrenLinkName(Class<? extends Element> targetClass)
   {
      return PROJECTSPRINTLINK;
   }
}
