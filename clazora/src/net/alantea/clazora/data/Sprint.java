package net.alantea.clazora.data;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import net.alantea.glide.Direction;
import net.alantea.glide.Element;
import net.alantea.glide.GException;
import net.alantea.glide.GlideElement;
import net.alantea.glide.Glider;
import net.alantea.glide.ParentElement;
import net.alantea.glide.PossibleChild;
import net.alantea.glide.Relation;
import net.alantea.horizon.message.Messenger;
import net.alantea.liteprops.Property;
import net.alantea.liteprops.StringProperty;

/**
 * The Class Sprint.
 */
public class Sprint extends GlideElement implements ParentElement
{
   
   /** The Constant SPRINTTASKSLINK. */
   private static final String SPRINTTASKSLINK = "sprintTasks";

   /** The Constant CHANGETASKS. */
   private static final String CHANGETASKS = "changeSprintTasks";
   
   /** The description. */
   private StringProperty description = new StringProperty("");
   
   /** The labels. */
   private List<String> labels;

   /** The start date. */
   private Property<Date> startDate = new Property<>(new Date());
   
   /** The due date. */
   private Property<Date> dueDate = new Property<>(new Date());
   
   /**
    * Instantiates a new sprint.
    */
   private Sprint()
   {
   }
   
   /**
    * Creates the sprint.
    *
    * @param glider the glider
    * @param title the title
    * @param description the description
    * @param startDate the start date
    * @param dueDate the due date
    * @return the sprint
    * @throws GException the g exception
    */
   public Sprint createSprint(Glider glider, String title, String description, Date startDate, Date dueDate) throws GException
   {
      Sprint sprint = glider.createEntity(Sprint.class);
      sprint.setTitle(title);
      sprint.setDescription(description);
      if (startDate != null)
      {
         sprint.setStartDate(startDate);
      }
      if (dueDate != null)
      {
         sprint.setDueDate(dueDate);
      }
      return sprint;
   }

   /**
    * Delete sprint.
    *
    * @param glider the glider
    * @param sprint the sprint
    * @throws GException the g exception
    */
   public static void deleteSprint(Glider glider, Sprint sprint) throws GException
   {
      if (sprint != null)
      {
         glider.removeEntity(sprint);
      }
   }

   /**
    * Gets the all sprints.
    *
    * @param glider the glider
    * @return the all sprints
    * @throws GException the g exception
    */
   public static List<Sprint> getAllSprints(Glider glider) throws GException
   {
      return glider.getClassEntities(Sprint.class.getName());
   }

   /**
    * Gets the title.
    *
    * @return the title
    */
   public String getTitle()
   {
      return getName();
   }

   /**
    * Sets the title.
    *
    * @param title the new title
    */
   public void setTitle(String title)
   {
      setName(title);
   }

   /**
    * Gets the description.
    *
    * @return the description
    */
   public String getDescription()
   {
      return description.get();
   }

   /**
    * Sets the description.
    *
    * @param description the new description
    */
   public void setDescription(String description)
   {
      this.description.set(description);
   }

   /**
    * Gets the start date.
    *
    * @return the start date
    */
   public Date getStartDate()
   {
      return startDate.get();
   }

   /**
    * Sets the start date.
    *
    * @param startDate the new start date
    */
   public void setStartDate(Date startDate)
   {
      this.startDate.set(startDate);
   }

   /**
    * Gets the labels.
    *
    * @return the labels
    */
   public List<String> getLabels()
   {
      return labels;
   }

   /**
    * Sets the labels.
    *
    * @param labels the new labels
    */
   public void setLabels(List<String> labels)
   {
      this.labels = labels;
   }

   /**
    * Gets the due date.
    *
    * @return the due date
    */
   public Date getDueDate()
   {
      return dueDate.get();
   }

   /**
    * Sets the due date.
    *
    * @param dueDate the new due date
    */
   public void setDueDate(Date dueDate)
   {
      this.dueDate.set(dueDate);
   }
   
   /**
    * Gets the tasks.
    *
    * @return the tasks
    */
   public List<Task> getTasks()
   {
      List<Task> ret = new LinkedList<>();
      List<Relation> list = this.getRelations(Direction.OUTGOING, SPRINTTASKSLINK);
      if (list != null)
      {
         for (Relation relation : list)
         {
            try
            {
               ret.add((Task) relation.getEndEntity());
            }
            catch (GException e)
            {
               e.printStackTrace();
            }
         }
      }
      return ret;
   }

   /**
    * Adds the task.
    *
    * @param task the task
    */
   public void addTask(Task task)
   {
      try
      {
         List<Relation> list = task.getRelations(Direction.INCOMING, SPRINTTASKSLINK);
         if (list != null)
         {
            for (Relation relation : list)
            {
               getGlider().removeRelation(relation);
            }
         }
         getGlider().createRelation(this, task, SPRINTTASKSLINK);
         Messenger.sendMessage(this, CHANGETASKS, task);
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Removes the task.
    *
    * @param task the task
    */
   public void removeTask(Task task)
   {
      try
      {
         List<Relation> list = task.getRelations(Direction.INCOMING, SPRINTTASKSLINK);
         if (list != null)
         {
            for (Relation relation : list)
            {
               getGlider().removeRelation(relation);
            }
         }
         Messenger.sendMessage(this, CHANGETASKS, task);
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Gets the children.
    *
    * @return the children
    * @throws GException the g exception
    */
   @Override
   public List<Element> getChildren() throws GException
   {
      List<Element> ret = new LinkedList<Element>();
      ret.addAll(getTasks());
      return ret;
   }

   /**
    * Gets the possible children.
    *
    * @return the possible children
    */
   @Override
   public List<PossibleChild> getPossibleChildren()
   {
      List<PossibleChild> ret = new LinkedList<>();
      ret.add(new PossibleChild(getGlider(), Task.class));
      return ret;
   }

   /**
    * Gets the children link name.
    *
    * @param targetClass the target class
    * @return the children link name
    */
   @Override
   public String getChildrenLinkName(Class<? extends Element> targetClass)
   {
      return SPRINTTASKSLINK;
   }
}
