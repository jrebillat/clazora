package net.alantea.clazora.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;
import net.alantea.utils.exception.LntException;

// TODO: Auto-generated Javadoc
/**
 * The Class Workflow.
 */
public class Workflow extends Entity
{
   
   /** The instance. */
   private static Workflow instance;
   
   /** The master task type name. */
   private String masterTaskTypeName;
   
   /** The key map. */
   private Map<String, String> keyMap = new HashMap<>();
   
   /**
    * Instantiates a new workflow.
    */
   private Workflow()
   {
   }

   /**
    * Gets the workflow.
    *
    * @param glider the glider
    * @return the workflow
    */
   private static Workflow getWorkflow(Glider glider)
   {
      try
      {
         if (instance == null)
         {
            List<Entity> list = glider.getClassEntities(Workflow.class.getName());
            if (list.isEmpty())
            {
               instance = glider.createEntity(Workflow.class);
            }
            else
            {
               instance = (Workflow) list.get(0);
            }
         }
      }
      catch (GException e)
      {
         new LntException("Error getting workflow", e);
      }
      return instance;
   }

   /**
    * Creates the task type.
    *
    * @param glider the glider
    * @param name the name
    * @param description the description
    * @param initialState the initial state
    * @return the task type
    */
   public static TaskType createTaskType(Glider glider, String name, String description, WorkflowState initialState)
   {
      try
      {
         TaskType taskType = glider.createEntity(TaskType.class);
         taskType.setName(name);
         taskType.setDescription(description);
         taskType.setStartingWorkflowState(initialState);
         return taskType;
      }
      catch (GException e)
      {
         new LntException("Error creating task type : " + name, e);
      }
      return null;
   }
   
   /**
    * Gets the task type.
    *
    * @param glider the glider
    * @param name the name
    * @return the task type
    */
   public static TaskType getTaskType(Glider glider, String name)
   {
      try
      {
         return TaskType.getTaskType(glider, name);
      }
      catch (GException e)
      {
         new LntException("Error getting task type : " + name, e);
      }
      return null;
   }

   /**
    * Gets the master task type.
    *
    * @param glider the glider
    * @return the master task type
    */
   public static TaskType getMasterTaskType(Glider glider)
   {
      getWorkflow(glider);
      try
      {
         return TaskType.getTaskType(glider, instance.masterTaskTypeName);
      }
      catch (GException e)
      {
         new LntException("Error getting master task type", e);
      }
      return null;
   }

   /**
    * Sets the master task type.
    *
    * @param glider the glider
    * @param masterTaskType the master task type
    */
   public static void setMasterTaskType(Glider glider, TaskType masterTaskType)
   {
      getWorkflow(glider);
      instance.masterTaskTypeName = masterTaskType.getName();
   }
   
   /**
    * Gets the sub task types.
    *
    * @param type the type
    * @return the sub task types
    */
   public static List<TaskType> getSubTaskTypes(TaskType type)
   {
      return type.getSubTaskTypes();
   }
   
   /**
    * Allow sub task type.
    *
    * @param type the type
    * @param subtype the subtype
    */
   public static void allowSubTaskType(TaskType type, TaskType subtype)
   {
      type.allowSubType(subtype);
   }
   
   /**
    * Gets the value.
    *
    * @param glider the glider
    * @param key the key
    * @return the value
    */
   public static String getValue(Glider glider, String key)
   {
      getWorkflow(glider);
      return instance.keyMap.get(key);
   }
   
   /**
    * Sets the value.
    *
    * @param glider the glider
    * @param key the key
    * @param value the value
    */
   public static void setValue(Glider glider, String key, String value)
   {
      getWorkflow(glider);
      instance.keyMap.put(key, value);
   }

}
