package net.alantea.clazora.data;

import net.alantea.glide.Element;
import net.alantea.glide.GException;
import net.alantea.glide.GlideElement;
import net.alantea.glide.Glider;
import net.alantea.glide.PossibleChild;

/**
 * The Class PossibleSubTask.
 */
public class PossibleSubTask extends PossibleChild
{
   
   /** The glider. */
   private Glider glider;
   
   /** The name. */
   private String name;
   
   /**
    * Instantiates a new possible sub task.
    *
    * @param glider the glider
    * @param typeName the type name
    */
   public PossibleSubTask(Glider glider, String typeName)
   {
      super(glider, Task.class);
      this.glider = glider;
      this.name = typeName;
   }
   
   /**
    * Gets the simple name.
    *
    * @return the simple name
    */
   public String getSimpleName()
   {
      return name;
   }

   /**
    * Creates the instance.
    *
    * @return the glide element
    */
   public GlideElement createInstance()
   {
      Task ret = null;
      try
      {
         ret = glider.createEntity(Task.class);
         TaskType type = TaskType.getTaskType(glider, name);
         if (type != null)
         {
            ret.setType(type);
         }
         Task.changeTaskState(glider, ret, type.getStartingWorkflowState());
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
      return ret;
   }

   /**
    * Gets the creation class.
    *
    * @return the creation class
    */
   public Class<? extends Element> getCreationClass()
   {
      return Task.class;
   }
}
