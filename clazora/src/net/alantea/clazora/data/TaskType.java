package net.alantea.clazora.data;

import java.util.LinkedList;
import java.util.List;

import net.alantea.glide.Direction;
import net.alantea.glide.GException;
import net.alantea.glide.GlideElement;
import net.alantea.glide.Glider;
import net.alantea.glide.Relation;
import net.alantea.utils.exception.LntException;

// TODO: Auto-generated Javadoc
/**
 * The Class TaskType.
 */
public class TaskType extends GlideElement
{
   
   /** The Constant STARTINGSTATE. */
   private static final String STARTINGSTATE = "startingState";
   
   /** The Constant ALLOWEDSUBTYPE. */
   private static final String ALLOWEDSUBTYPE = "allowedSubtype";
   
   /** The description. */
   private String description;
   
   /** The create label. */
   private String createLabel = null;
   
   /** The remove label. */
   private String removeLabel = null;

   /**
    * Instantiates a new task type.
    */
   private TaskType()
   {
   }
   
   /**
    * Creates the task type.
    *
    * @param glider the glider
    * @param name the name
    * @return the task type
    * @throws GException the g exception
    */
   public static TaskType createTaskType(Glider glider, String name) throws GException
   {
      return createTaskType(glider, name, "");
   }
   
   /**
    * Creates the task type.
    *
    * @param glider the glider
    * @param name the name
    * @param description the description
    * @return the task type
    * @throws GException the g exception
    */
   public static TaskType createTaskType(Glider glider, String name,
         String description) throws GException
   {
      TaskType taskType = glider.createEntity(TaskType.class);
      taskType.setName(name);
      taskType.setDescription(description);
      
      return taskType;
   }
   
   /**
    * Gets the task types.
    *
    * @param glider the glider
    * @return the task types
    * @throws GException the g exception
    */
   public static List<TaskType> getTaskTypes(Glider glider) throws GException
   {
      List<TaskType> taskTypes = glider.getClassEntities(TaskType.class.getName());
      return taskTypes;
   }
   
   /**
    * Gets the task type.
    *
    * @param glider the glider
    * @param name the name
    * @return the task type
    * @throws GException the g exception
    */
   public static TaskType getTaskType(Glider glider, String name) throws GException
   {
      List<TaskType> taskTypes = glider.getClassEntities(TaskType.class.getName());
      for (TaskType type : taskTypes)
      {
         if (type.getName().equals(name))
         {
            return type;
         }
      }
      return null;
   }
   
   /**
    * Gets the description.
    *
    * @return the description
    */
   public String getDescription()
   {
      return description;
   }
   
   /**
    * Sets the description.
    *
    * @param description the new description
    */
   public void setDescription(String description)
   {
      this.description = description;
   }
   
   /**
    * Gets the creates the label.
    *
    * @return the creates the label
    */
   public String getCreateLabel()
   {
      return createLabel;
   }

   /**
    * Sets the creates the label.
    *
    * @param createLabel the new creates the label
    */
   public void setCreateLabel(String createLabel)
   {
      this.createLabel = createLabel;
   }

   /**
    * Gets the removes the label.
    *
    * @return the removes the label
    */
   public String getRemoveLabel()
   {
      return removeLabel;
   }

   /**
    * Sets the removes the label.
    *
    * @param removeLabel the new removes the label
    */
   public void setRemoveLabel(String removeLabel)
   {
      this.removeLabel = removeLabel;
   }

   /**
    * Gets the starting workflow state.
    *
    * @return the starting workflow state
    */
   public WorkflowState getStartingWorkflowState()
   {
      List<Relation> list = this.getRelations(Direction.OUTGOING, STARTINGSTATE);
      if (!list.isEmpty())
      {
         try
         {
            return (WorkflowState) list.get(0).getEndEntity();
         }
         catch (GException e)
         {
            e.printStackTrace();
         }
      }
      return null;
   }
   
   /**
    * Sets the starting workflow state.
    *
    * @param state the new starting workflow state
    */
   public void setStartingWorkflowState(WorkflowState state)
   {
      if (state != null)
      {
         try
         {
            List<Relation> list = this.getRelations(Direction.OUTGOING, STARTINGSTATE);
            if (!list.isEmpty())
            {
               for (Relation relation : list)
               {
                  getGlider().removeRelation(relation);
               }
            }
            this.getGlider().createRelation(this, state, STARTINGSTATE);
         }
         catch (GException e)
         {
            e.printStackTrace();
         }
      }
   }

   /**
    * Allow sub type.
    *
    * @param subtype the subtype
    */
   public void allowSubType(TaskType subtype)
   {
      if (subtype != null)
      {
         try
         {
            this.getGlider().createRelation(this, subtype, ALLOWEDSUBTYPE);
         }
         catch (GException e)
         {
            new LntException("Error allow sub type : " + subtype.getName() + " for " + getName(), e);
         }
      }
   }
   
   /**
    * Gets the sub task types.
    *
    * @return the sub task types
    */
   public List<TaskType> getSubTaskTypes()
   {
      try
      {
         return getGlider().getRelatedEntities(this, Direction.OUTGOING, ALLOWEDSUBTYPE);
      }
      catch (GException e)
      {
         new LntException("Error getting sub types for " + getName(), e);
      }
      return new LinkedList<TaskType>();
   }
}
